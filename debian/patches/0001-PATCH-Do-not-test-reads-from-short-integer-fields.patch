From: Zygmunt Krynicki <me@zygoon.pl>
Date: Fri, 26 Jun 2020 19:58:55 +0200
Subject: [PATCH] Do not test reads from short integer fields

The automatic promotion process from ZT_INTEGER to ZT_INTMAX, and from
ZT_UNSIGED to ZT_UINTMAX, was correct but the test measuring that was
flawed, as it relied on reading the shorter words instead. On some
architectures, notably Sparc 64, this results in a read of the word
zeroed by the write to the wider variant.

Signed-off-by: Zygmunt Krynicki <me@zygoon.pl>
---
 zt-test.c | 8 ++++----
 zt.h      | 4 ++--
 2 files changed, 6 insertions(+), 6 deletions(-)

diff --git a/zt-test.c b/zt-test.c
index db823db..b68ab76 100644
--- a/zt-test.c
+++ b/zt-test.c
@@ -1669,7 +1669,7 @@ static void test_ZT_CMP_INT(void)
     assert(claim.make_verifier == zt_verifier_for_integer_relation);
 
     assert(zt_value_kind_of(claim.args[0]) == ZT_INTMAX);
-    assert(claim.args[0].as.integer == 1);
+    assert(claim.args[0].as.intmax == 1);
     assert(strcmp(zt_source_of(claim.args[0]), "a") == 0);
 
     assert(zt_value_kind_of(claim.args[1]) == ZT_STRING);
@@ -1677,7 +1677,7 @@ static void test_ZT_CMP_INT(void)
     assert(strcmp(zt_source_of(claim.args[1]), "==") == 0);
 
     assert(zt_value_kind_of(claim.args[2]) == ZT_INTMAX);
-    assert(claim.args[2].as.integer == -2);
+    assert(claim.args[2].as.intmax == -2);
     assert(strcmp(zt_source_of(claim.args[2]), "b") == 0);
 }
 
@@ -1721,7 +1721,7 @@ static void test_ZT_CMP_UINT(void)
     assert(claim.make_verifier == zt_verifier_for_unsigned_relation);
 
     assert(zt_value_kind_of(claim.args[0]) == ZT_UINTMAX);
-    assert(claim.args[0].as.unsigned_integer == 1);
+    assert(claim.args[0].as.uintmax == 1);
     assert(strcmp(zt_source_of(claim.args[0]), "a") == 0);
 
     assert(zt_value_kind_of(claim.args[1]) == ZT_STRING);
@@ -1729,7 +1729,7 @@ static void test_ZT_CMP_UINT(void)
     assert(strcmp(zt_source_of(claim.args[1]), "==") == 0);
 
     assert(zt_value_kind_of(claim.args[2]) == ZT_UINTMAX);
-    assert(claim.args[2].as.unsigned_integer == 2);
+    assert(claim.args[2].as.uintmax == 2);
     assert(strcmp(zt_source_of(claim.args[2]), "b") == 0);
 }
 
diff --git a/zt.h b/zt.h
index bc6f205..9ca2874 100644
--- a/zt.h
+++ b/zt.h
@@ -64,8 +64,8 @@ typedef struct zt_value {
     union {
         bool boolean;
         int rune;
-        int integer;
-        unsigned unsigned_integer;
+        int integer; /* Deprecated. */
+        unsigned unsigned_integer; /* Deprecated. */
         const char* string;
         const void* pointer;
         intmax_t intmax;
